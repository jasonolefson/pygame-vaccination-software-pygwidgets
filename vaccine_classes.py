# Import necessary modules
from abc import ABC, abstractmethod

# Person Class
class Person(ABC):
  def __init__(self, first_name, last_name):
    self.first_name = first_name
    self.last_name = last_name
    
    @abstractmethod
    def report(self):
      pass
    
    

# PATIENT CLASS
class patient(Person):

  def __init__(self, first_name, last_name, hasVacA=False, hasVacB=False, hasVacC=False, hasSymptA=False, hasSymptB=False, hasSymptC=False):
    super().__init__(first_name, last_name)
    # object properties
    self.hasVacA = hasVacA
    self.hasVacB = hasVacB
    self.hasVacC = hasVacC
    self.hasSymptA = hasSymptA
    self.hasSymptB = hasSymptB
    self.hasSymptC = hasSymptC
    
  # object methods
  def report(self):  # reports vaccination data for individual
    print(f"{self.first_name} {self.last_name} has the following vaccination status: ")
    print(f"Vaccine A: {self.hasVacA}, Vaccine B: {self.hasVacB}, Vaccine C: {self.hasVacC}")
    print(f"Symptoms: A: {self.hasSymptA}, B: {self.hasSymptB}, C: {self.hasSymptC}")
    

  # Toggle Vaccines
  def vac_AToggle(self):
    self.hasVacA = not self.hasVacA

  def vac_BToggle(self):
    self.hasVacB = not self.hasVacB

  def vac_CToggle(self):
    self.hasVacC = not self.hasVacC
  
  # Toggle Symptoms
  def sympt_AToggle(self):
    self.hasSymptA = not self.hasSymptA
  
  def sympt_BToggle(self):
    self.hasSymptB = not self.hasSymptB
    
  def sympt_CToggle(self):
    self.hasSymptC = not self.hasSymptC
  
  # Set Names
  def setFirstName(self, name):
    self.pFname = name
  
  def setLastName(self, name):
    self.pLname = name

# PATIENT LIST CLASS (Patient Manager Object)
class patient_list():

  def __init__(self):
    self.patients = []

  def add_patient(self, patient):
    self.patients.append(patient)

  def report_totals(self):  #Report total vaccination status
    vacACount = 0
    vacBCount = 0
    vacCCount = 0
    for i in range(len(self.patients)):
      if self.patients[i].hasVacA == True:
        vacACount += 1
      if self.patients[i].hasVacB == True:
        vacBCount += 1
      if self.patients[i].hasVacC == True:
        vacCCount += 1
    return(
      f"vac_A total: {vacACount}, vac_B total: {vacBCount}, vac_C total: {vacCCount}, "
    )
    
  def report_sympt_totals(self): # report 
        symptA_VacA_Count = 0
        symptB_VacA_Count = 0
        symptC_VacA_Count = 0
        symptA_VacB_Count = 0
        symptB_VacB_Count = 0
        symptC_VacB_Count = 0
        symptA_VacC_Count = 0
        symptB_VacC_Count = 0
        symptC_VacC_Count = 0

        for patient in self.patients:
            if patient.hasVacA:
                if patient.hasSymptA:
                    symptA_VacA_Count += 1
                if patient.hasSymptB:
                    symptB_VacA_Count += 1
                if patient.hasSymptC:
                    symptC_VacA_Count += 1

            if patient.hasVacB:
                if patient.hasSymptA:
                    symptA_VacB_Count += 1
                if patient.hasSymptB:
                    symptB_VacB_Count += 1
                if patient.hasSymptC:
                    symptC_VacB_Count += 1

            if patient.hasVacC:
                if patient.hasSymptA:
                    symptA_VacC_Count += 1
                if patient.hasSymptB:
                    symptB_VacC_Count += 1
                if patient.hasSymptC:
                    symptC_VacC_Count += 1

        return(f"VacA Total sympt: A = {symptA_VacA_Count} B = {symptB_VacA_Count} C = {symptC_VacA_Count}... VacB Total sympt: A = {symptA_VacB_Count} B = {symptB_VacB_Count} C = {symptC_VacB_Count}... VacC Total sympt: A = {symptA_VacC_Count} B = {symptB_VacC_Count} C = {symptC_VacC_Count}")

    
  def reset_patients(self):
    for patient in self.patients:
      patient.hasVacA = False
      patient.hasVacB = False
      patient.hasVacC = False
      patient.hasSymptA = False
      patient.hasSymptB = False
      patient.hasSymptC = False
        
  def clear_patient_list(self):
      self.patients = []  # Clear the list of patients

# Create global patient_list
patientList = patient_list()