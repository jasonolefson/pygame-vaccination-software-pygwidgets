from vaccine_classes import *
import pygame
import pygwidgets
from abc import ABC, abstractmethod

# Create Display window
WINDOW_HEIGHT = 500
WINDOW_WIDTH = 1400
pygame.init()
window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption('GUI DEMO')
timer = pygame.time.Clock() #load clock
fps = 60 #set fps
font = pygame.font.Font('freesansbold.ttf', 18) #load font

# Game Variables
menu_state ="main" #This will allow state management of menus

# Abstract Button Class
class Button(ABC):
    def __init__(self, window, loc, **kwargs):
        self.window = window
        self.loc = loc
        self.kwargs = kwargs
        self.is_hovered = False
        self.is_clicked = False
    
    # ABSTRACT METHODS
    @abstractmethod
    def draw(self):
        pass
    
    @abstractmethod
    def handleEvent(self, event):
        pass

# TextButton Subclas, POLYMORPHISM
class TextButton(Button):
    def __init__(self, window, loc, text, **kwargs):
        super().__init__(window, loc, **kwargs)
        self.text = text
        self.width = kwargs.get('width', None)
        self.height = kwargs.get('height', 40)
        self.textColor = kwargs.get('textColor', (0, 0, 0))
        self.upColor = kwargs.get('upColor', (170, 170, 170))
        self.overColor = kwargs.get('overColor', (210, 210, 210))
        self.downColor = kwargs.get('downColor', (140, 140, 140))
        self.fontName = kwargs.get('fontName', None)
        self.fontSize = kwargs.get('fontSize', 20)
        self.soundOnClick = kwargs.get('soundOnClick', None)
        self.enterToActivate = kwargs.get('enterToActivate', False)
        self.callBack = kwargs.get('callBack', None)
        self.nickname = kwargs.get('nickname', None)
        self.activationKeysList = kwargs.get('activationKeysList', None)
        self.rect = pygame.Rect(loc[0], loc[1], self.width, self.height)
    
    def is_over(self):
        return self.rect.collidepoint(pygame.mouse.get_pos())
    
    def is_down(self):
        return pygame.mouse.get_pressed()[0] and self.is_over()
    
    
    def draw(self):
        if self.is_down():
            color = self.upColor
        elif self.is_over():
            color = self.overColor
        else:
            color = self.upColor
            
        pygame.draw.rect(self.window, color, self.rect)
        # Draw text on button
        text_surface = font.render(self.text, True, self.textColor)
        text_rect = text_surface.get_rect(center=self.rect.center)
        self.window.blit(text_surface, text_rect)
    
    def handleEvent(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.is_clicked = True
        elif event.type == pygame.MOUSEBUTTONUP:
            self.is_clicked = False
            if self.rect.collidepoint(event.pos):
                # Execute the callback function if it exists
                if self.kwargs.get('callBack'):
                    self.kwargs['callBack']()
    
# Patient Info Display Class
class PatientInfoDisplay:
    def __init__(self, x, y, patient):
        self.x = x
        self.y = y
        self.patient = patient

    def draw(self, window):
        # Display first name
        text_surface = font.render(self.patient.first_name, True, 'black')
        window.blit(text_surface, (self.x, self.y))

        # Display last name
        text_surface = font.render(self.patient.last_name, True, 'black')
        window.blit(text_surface, (self.x + 150, self.y))

        # Display vaccine statuses
        vax_a_status = "True" if self.patient.hasVacA else "False"
        text_surface = font.render(vax_a_status, True, 'black')
        window.blit(text_surface, (self.x + 300, self.y))

        vax_b_status = "True" if self.patient.hasVacB else "False"
        text_surface = font.render(vax_b_status, True, 'black')
        window.blit(text_surface, (self.x + 400, self.y))

        vax_c_status = "True" if self.patient.hasVacC else "False"
        text_surface = font.render(vax_c_status, True, 'black')
        window.blit(text_surface, (self.x + 500, self.y))

        # Display symptom statuses
        sympt_a_status = "True" if self.patient.hasSymptA else "False"
        text_surface = font.render(sympt_a_status, True, 'black')
        window.blit(text_surface, (self.x + 600, self.y))

        sympt_b_status = "True" if self.patient.hasSymptB else "False"
        text_surface = font.render(sympt_b_status, True, 'black')
        window.blit(text_surface, (self.x + 700, self.y))

        sympt_c_status = "True" if self.patient.hasSymptC else "False"
        text_surface = font.render(sympt_c_status, True, 'black')
        window.blit(text_surface, (self.x + 800, self.y))
        
# BUTTONS
# main menu buttons
input_button = pygwidgets.TextButton(window, (140, 420), "Input Data")
rep_patient_button = pygwidgets.TextButton(window, (270, 420), "Patient Info")
rep_vaccs_button = pygwidgets.TextButton(window, (400, 420), "Rep vaccs")
rep_sympts_button = pygwidgets.TextButton(window, (530, 420), "Rep Sympts")
reset_button = pygwidgets.TextButton(window, (660, 420), "Reset Data")

# Custom buttons
exit_button = TextButton(window, (1250, 10), "Exit", width=80, height=40, callBack=pygame.quit)


# input menu buttons
back_button = pygwidgets.TextButton(window, (10, 10), "Back")
submit_button = pygwidgets.TextButton(window, (650, 420), "Submit")

# patient info menu buttons
prev_button = pygwidgets.TextButton(window, (450, 420), "Prev.")
next_button = pygwidgets.TextButton(window, (650, 420), "Next")
search_button = pygwidgets.TextButton(window, (1070, 420), "Seach Patient No.")

# Records Input Boxes
first_name_box = pygwidgets.InputText(window, (40, 75), width=140)
last_name_box = pygwidgets.InputText(window, (235, 75), width=140)

# Patient info Input Boxes
search_input_box = pygwidgets.InputText(window, (1000, 400))
    
# Display Texts

# Input Data Display Text
display_first_name = pygwidgets.DisplayText(window, (50, 50), value="First Name")
display_last_name = pygwidgets.DisplayText(window, (245, 50), value="Last Name")
patient_1_text = pygwidgets.DisplayText(window, (10, 80), value="0.")
Vax_A = pygwidgets.DisplayText(window, (400, 50), value="Vax_A")
Vax_B = pygwidgets.DisplayText(window, (500, 50), value="Vax_B")
Vax_C = pygwidgets.DisplayText(window, (600, 50), value="Vax_C")
Sympt_A = pygwidgets.DisplayText(window, (700, 50), value="Sympt_A")
Sympt_B = pygwidgets.DisplayText(window, (800, 50), value="Sympt_B")
Sympt_C = pygwidgets.DisplayText(window, (900, 50), value="Sympt_C")
display_first_name_2 = pygwidgets.DisplayText(window, (75, 50), value="First Name")


labels_to_draw = [display_first_name, display_last_name, patient_1_text, Vax_A, Vax_B, Vax_C, Sympt_A, Sympt_B, Sympt_C]
p_info_labels_to_draw = [display_first_name_2, display_last_name, Vax_A, Vax_B, Vax_C, Sympt_A, Sympt_B, Sympt_C]

# Main Display Text
vacc_totals_text = pygwidgets.DisplayText(window, (400, 200), value="", fontSize=30)
sympt_totals_text = pygwidgets.DisplayText(window, (150, 100), value="", fontSize=30)


# Toggle Buttons for Patient
vax_a_button_p1 = pygwidgets.TextCheckBox(window, (Vax_A.getLoc()[0] + 30, patient_1_text.getLoc()[1] + 5), value=False, text="")
vax_b_button_p1 = pygwidgets.TextCheckBox(window, (Vax_B.getLoc()[0] + 30, patient_1_text.getLoc()[1] + 5), value=False, text="")
vax_c_button_p1 = pygwidgets.TextCheckBox(window, (Vax_C.getLoc()[0] + 30, patient_1_text.getLoc()[1] + 5), value=False, text="")
symp_a_button_p1 = pygwidgets.TextCheckBox(window, (Sympt_A.getLoc()[0] + 30, patient_1_text.getLoc()[1] + 5), value=False, text="")
symp_b_button_p1 = pygwidgets.TextCheckBox(window, (Sympt_B.getLoc()[0] + 30, patient_1_text.getLoc()[1] + 5), value=False, text="")
symp_c_button_p1 = pygwidgets.TextCheckBox(window, (Sympt_C.getLoc()[0] + 30, patient_1_text.getLoc()[1] + 5), value=False, text="")


toggle_buttons = [
    vax_a_button_p1, vax_b_button_p1, vax_c_button_p1, symp_a_button_p1, symp_b_button_p1, symp_c_button_p1
]


# Other external vars
patient_info_display_p1 = None

# Game Loop
run = True

while run:
    window.fill((202, 228, 241))
    timer.tick(fps)
        
    
    # Main Menu
    if menu_state == "main":
        input_button.draw()
        rep_patient_button.draw()
        rep_vaccs_button.draw()
        rep_sympts_button.draw()
        vacc_totals_text.draw()
        sympt_totals_text.draw()
        reset_button.draw()
        
        # Main Event Handlers
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if input_button.handleEvent(event): #check if button click
                menu_state = "records"
            if rep_patient_button.handleEvent(event):
                menu_state = "patient_info"
            if rep_vaccs_button.handleEvent(event):
                patientList.report_totals()
                vacc_totals_text.setValue(patientList.report_totals())
                pygame.display.update()
            if rep_sympts_button.handleEvent(event):
                patientList.report_sympt_totals()
                sympt_totals_text.setValue(patientList.report_sympt_totals())
                pygame.display.update()
            if event.type == pygame.QUIT:
                run = False    
            exit_button.handleEvent(event)
            # Reset Button Hanbdlers
            if reset_button.handleEvent(event):
                
                # Toggle off (set to red) all toggle buttons
                for toggle_button in toggle_buttons:
                    toggle_button.value = False
                   

                # Clear patient list
                patientList.reset_patients()
                patient_info_display_p1 = None
                patientList.clear_patient_list()
                patient_1_text.setValue("0.")
                print("Data reset successfully.")
                
    # Records Menu
    if menu_state == "records":
        back_button.draw()
        submit_button.draw()
        
        # Draw User Input Boxes
        first_name_box.draw()
        last_name_box.draw()
        
        # Draw Toggle Buttons
        for toggle_button in toggle_buttons:
            toggle_button.draw()
        
        # Draw Labels
        for label in labels_to_draw:
            label.draw()
     
        
        
        # Records Event Handlers
        for event in pygame.event.get():
            if back_button.handleEvent(event):
                menu_state = "main"
            if exit_button.handleEvent(event):
                run = False
            first_name_box.handleEvent(event)
            last_name_box.handleEvent(event)
            
            # SUBMIT BUTTON HANDLERS
            if submit_button.handleEvent(event):
    # Prepare data for add_patient method
                first_name = first_name_box.text
                last_name = last_name_box.text
                toggle_states = [toggle_button.value for toggle_button in toggle_buttons]
                patient_1_text.setValue(str(len(patientList.patients) + 1) + ".")

                # Use the modified add_patient method
                person_to_add = patient(first_name, last_name, *toggle_states)
                patientList.add_patient(person_to_add)
                print("Patient data submitted successfully.")

                # Clear input text boxes for the next entry
                first_name_box.setValue("")
                last_name_box.setValue("")

                # Toggle off (set to red) all toggle buttons for the next entry
                for toggle_button in toggle_buttons:
                    toggle_button.value = False
                
            # ToggleButton Events
            for toggle_button in toggle_buttons:
                toggle_button.handleEvent(event)
    
    # Patient Info Menu
    if menu_state == "patient_info":
        back_button.draw()
        prev_button.draw()
        next_button.draw()
        search_button.draw()
        search_input_box.draw()
        # Check if the patient list is not empty
        if patientList.patients:
            if patient_info_display_p1 is None:
                patient_info_display_p1 = PatientInfoDisplay(100, 100, patientList.patients[0])
            if not hasattr(patient_info_display_p1, 'index'):
                # Initialize the index if it doesn't exist
                patient_info_display_p1.index = 0

            # Draw patient info at the center of the window
            if patient_info_display_p1 == None:
                pass
            else:
                patient_info_display_p1.draw(window)
            
            for label in p_info_labels_to_draw:
                label.draw()
    
        # Patient Info Event Handlers
        for event in pygame.event.get():
            if back_button.handleEvent(event):
                menu_state = "main"
            if exit_button.handleEvent(event):
                run = False
            if search_button.handleEvent(event):
                try:
                    # Get the entered index from the TextBox and update PatientInfoDisplay
                    entered_index = int(search_input_box.text)
                    if 0 <= entered_index < len(patientList.patients):
                        patient_info_display_p1.index = entered_index
                        patient_info_display_p1.patient = patientList.patients[entered_index]
                        pygame.display.update()
                    else:
                        print("Invalid index. Please enter a valid index.")
                except ValueError:
                    print("Invalid input. Please enter a valid index.")

            # TextBox Event Handler
            search_input_box.handleEvent(event)
            if patientList.patients:
                if prev_button.handleEvent(event):
                        patient_info_display_p1.index = (patient_info_display_p1.index - 1) % len(patientList.patients)
                        patient_info_display_p1.patient = patientList.patients[patient_info_display_p1.index]
                        pygame.display.update()
                        
                if next_button.handleEvent(event):
                        patient_info_display_p1.index = (patient_info_display_p1.index + 1) % len(patientList.patients)
                        patient_info_display_p1.patient = patientList.patients[patient_info_display_p1.index]
                        pygame.display.update()
                        
            # Error handle if list is None
            if patient_info_display_p1 == None:
                pass
            else:
                patient_info_display_p1.draw(window)

        
    exit_button.draw() # Stand-Alone exit button
    # Draw window elements
    pygame.display.update()

pygame.quit()